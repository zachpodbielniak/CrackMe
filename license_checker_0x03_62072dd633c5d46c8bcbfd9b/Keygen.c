/* Solves https://crackmes.one/crackme/62072dd633c5d46c8bcbfd9b *most* of the time */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define EXPECTED_VALUE		0x32




static
void 
__Setup(
	void
){ srand(time(NULL)); }




static
void
__IterateRandom(
	char		*csBuffer,
	int		iSizeOfBuffer
){
	char csTemp[0x100];
	int iRand;

	if (NULL == csBuffer || 0 == iSizeOfBuffer)
	{ exit (1); }

	memset(csTemp, 0x00, sizeof(csTemp));

	iRand = rand() % 10;
	snprintf(csTemp, sizeof(csTemp) - 1, "%d", iRand);
	strncat(csBuffer, csTemp, iSizeOfBuffer);
}



/* 
	Code reverse engineered from binary, tweaked
	to be used in a modular fashion as the secret
	value generator
*/
static 
int 
__GetSecretValue(
	char		*csBuffer,
	int 		*piCompare,
	int		*piIncrement
){
	char *szTemp;
	int iInputLength;
	
	iInputLength = strlen(csBuffer);
	if (iInputLength <= *piIncrement) 
	{ return -1; }

	szTemp = *(char *)((long)*piIncrement + csBuffer);
	iInputLength = atoi(&szTemp);
	*piCompare = *piCompare + iInputLength;
	*piIncrement = *piIncrement + 1;

	return 0;
}




int
main(
    int     argc,
    char    **argv
){
	char csBuffer[0x100];
	int iSuccess;
	int iCompare;
	int iIncrement;

	__Setup();
	
	__Restart:
	iCompare = 0;
	iIncrement = 0;
	memset(csBuffer, 0x00, sizeof(csBuffer));

	for (;;) 
	{
		__IterateRandom(
			csBuffer,
			sizeof(csBuffer) - 1
		);

		iSuccess = __GetSecretValue(
			csBuffer,
			&iCompare,
			&iIncrement
		);

		if (-1 == iSuccess)
		{ break; }


		if (EXPECTED_VALUE < iCompare)
		{ goto __Restart; }
		else if (EXPECTED_VALUE == iCompare)
		{ break; }
	}

	printf("%s", csBuffer);
	return 0;
}